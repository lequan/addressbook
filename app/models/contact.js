AddressBook.Models.Contact = Backbone.Model.extend({
    defaults: {
        firstName: null,
        lastName: null,
        email: null,
        tel: null,
        dob: null,
        description: null,
        isFavorite: null
    }
});