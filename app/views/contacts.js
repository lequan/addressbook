AddressBook.Views.Contacts = Backbone.View.extend({
    template: _.template($('#tpl-contacts').html()),

    events: {
        'click .sort-by-first-name': 'sortByFirstName',
        'click .sort-by-tel': 'sortByTel'
    },

    renderOne: function(contact) {
        var me = this,
            itemView = new AddressBook.Views.Contact({model: contact});
        me.$('.contacts-container').append(itemView.render().$el);
    },

    render: function() {
        var me = this,
            html = me.template();
        me.$el.html(html);
        me.collection.each(function (contact) {
            me.renderOne(contact);
        }, me);
        return me;
    },

    sortByFirstName: function (e) {
        var me = this,
            sortedModels;
        e.preventDefault();
        sortedModels = _.sortBy(me.collection.models, function (obj) {
            return obj.attributes.firstName;
        });
        me.collection.models = sortedModels;
        me.trigger('contactsList:sorted');
    },

    sortByTel: function (e) {
        var me = this,
            sortedModels;
        e.preventDefault();
        sortedModels = _.sortBy(me.collection.models, function (obj) {
            return obj.attributes.tel;
        });
        me.collection.models = sortedModels;
        me.trigger('contactsList:sorted');
    }
});