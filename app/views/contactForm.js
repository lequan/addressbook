AddressBook.Views.ContactForm = Backbone.View.extend({
    template: _.template($('#tpl-new-contact').html()),

    events: {
        'submit .contract-form': 'onFormSubmit'
    },

    render: function() {
        var me = this,
            html = me.template(_.extend(me.model.toJSON(), {isNew: me.model.isNew()}));
        me.$el.append(html);
        return me;
    },

    onFormSubmit: function(e) {
        var me = this;
        e.preventDefault();
        me.trigger('form:submitted', {
            firstName: me.$('.contact-first-name-input').val(),
            lastName: me.$('.contact-last-name-input').val(),
            email: me.$('.contact-email-input').val(),
            tel: me.$('.contact-tel-input').val(),
            dob: me.$('.contact-dob-input').val(),
            description: me.$('.contact-description-input').val()
        });
    }
});