AddressBook.Views.Contact = Backbone.View.extend({
    tagName: 'li',
    className: 'media col-md-4 col-lg-4',
    template: _.template($('#tpl-contact').html()),

    events: {
        'click .delete-contact': 'onClickDelete'
    },

    initialize: function() {
        var me = this;
        me.listenTo(me.model, 'remove', me.remove);
    },

    render: function() {
        var me = this,
            html = me.template(me.model.toJSON());
        me.$el.append(html);
        return me;
    },

    onClickDelete: function(e) {
        var me = this;
        e.preventDefault();
        me.model.collection.remove(me.model);
    }
});