AddressBook.Router = Backbone.Router.extend({
    routes: {
        '': 'home',
        'contacts': 'showContacts',
        'favorites': 'showFavorites',
        'contacts/new': 'newContact',
        'contacts/open/:id': 'openContact',
        'contacts/changeIsFavorite/:id': 'changeIsFavorite'
    }
});