window.AddressBook = {
    Models: {},
    Collections: {},
    Views: {},

    start: function(ctx) {
        var inpContacts = ctx.contacts,
            contacts = new AddressBook.Collections.Contacts(inpContacts),
            favoriteContacts, favoriteContactsArray,
            router = new AddressBook.Router();
        // Router
        router.on('route:home', function () {
            router.navigate('contacts', {
                trigger: true,
                replace: true
            });
        });
        router.on('route:showContacts', function () {
            var contactsView = new AddressBook.Views.Contacts({
                collection: contacts
            });
            contactsView.on('contactsList:sorted', function() {
                this.render();
            });
            // Render contacts view
            $('.main-container').html(contactsView.render().$el);
        });
        router.on('route:showFavorites', function () {
            favoriteContactsArray = [];
            contacts.models.forEach(function (contact) {
                if (contact.attributes.isFavorite) {
                    favoriteContactsArray.push(contact);
                }
            });
            favoriteContacts = new AddressBook.Collections.FavoriteContacts(favoriteContactsArray);
            var favoriteContactsView = new AddressBook.Views.Contacts({
                collection: favoriteContacts
            });
            favoriteContactsView.on('contactsList:sorted', function() {
                this.render();
            });
            // Render contacts view
            $('.main-container').html(favoriteContactsView.render().$el);
        });
        router.on('route:newContact', function () {
            var newContactForm = new AddressBook.Views.ContactForm({
                model: new AddressBook.Models.Contact()
            });
            newContactForm.on('form:submitted', function(attrs) {
                attrs.id = contacts.isEmpty() ? 1 : (_.max(contacts.pluck('id')) + 1);
                contacts.add(attrs);
                router.navigate('contacts', true);
            });
            $('.main-container').html(newContactForm.render().$el);
        });
        router.on('route:openContact', function (id) {
            var contact = contacts.get(id),
                openContactForm;
            if (contact) {
                openContactForm = new AddressBook.Views.ContactForm({
                    model: contact
                });
                openContactForm.on('form:submitted', function(attrs) {
                    contact.set(attrs);
                    router.navigate('contacts', true);
                });
                $('.main-container').html(openContactForm.render().$el);
            } else {
                router.navigate('contacts', true);
            }
        });
        router.on('route:changeIsFavorite', function (id) {
            var contact = contacts.get(id);
            if (contact) {
                contact.attributes.isFavorite = !contact.attributes.isFavorite;
            }
            router.navigate('contacts', true);
        });
        Backbone.history.start();
    }
};